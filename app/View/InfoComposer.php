<?php

namespace App\View;

use App\Domain\Cameras\Repositories\CameraRepository;
use App\Domain\Sections\Repositories\SectionRepository;
use Illuminate\View\View;

class InfoComposer
{
    public function compose(View $view)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://uniwork.buxdu.uz/api/departments.asp');
        $departments = json_decode($response->getBody())->departments;
        $s = 0;
        for ($i=0; $i<count($departments); $i++){
            if($departments[$i]->sub_id != 'null'){
                $s++;
            }
        }

        $count_section = count(json_decode($response->getBody())->departments);
        $cameraRepository = new CameraRepository();
        $camera = $cameraRepository->getAllCount();

        $view->with([
            'section_count' => $count_section,
            'sub_section' => $s,
            'camera_count' => $camera,
        ]);
    }
}
