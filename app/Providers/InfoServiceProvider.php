<?php

namespace App\Providers;

use App\View\InfoComposer;
use Illuminate\Support\ServiceProvider;

class InfoServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('dashboard.layout.index', InfoComposer::class);
        view()->composer('web.user.camera', InfoComposer::class);
    }
}
