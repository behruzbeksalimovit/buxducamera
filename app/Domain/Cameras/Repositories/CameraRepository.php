<?php


namespace App\Domain\Cameras\Repositories;


use App\Domain\Cameras\Models\Camera;
use Illuminate\Database\Eloquent\Collection;

class CameraRepository
{
    /**
     * @return Camera[]|Collection
     */
    public function getAll()
    {
        return Camera::paginate();
    }

    public function getAllCount(){
        return Camera::count();
    }

    public function getWebAll()
    {
        return Camera::where('favorite','=', 1)->paginate();
    }

    public function all()
    {
        return Camera::all();
    }

//    rektor
//    public function rektorGetCameraAll()
//    {
//
//    }
}
