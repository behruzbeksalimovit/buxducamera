<?php


namespace App\Domain\Cameras\Actions;


use App\Domain\Cameras\DTO\StoreCameraDTO;
use App\Domain\Cameras\Models\Camera;
use Exception;
use Illuminate\Support\Facades\DB;

class StoreCameraAction
{
    /**
     * @param StoreCameraDTO $dto
     * @return Camera
     * @throws Exception
     */
    public function execute(StoreCameraDTO $dto)
    {
        DB::beginTransaction();
        try {
            $camera = new Camera();
            $camera->name = $dto->getName();
            $camera->link = $dto->getLink();
            $camera->section_id = $dto->getSectionId();
            $camera->favorite = $dto->getFavorite();
            $camera->save();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $camera;
    }
}
