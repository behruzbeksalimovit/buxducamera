<?php


namespace App\Domain\Cameras\Actions;


use App\Domain\Cameras\DTO\UpdateCameraDTO;
use App\Domain\Cameras\Models\Camera;
use Exception;
use Illuminate\Support\Facades\DB;

class UpdateCameraAction
{
    /**
     * @param UpdateCameraDTO $dto
     * @return Camera
     * @throws Exception
     */
    public function execute(UpdateCameraDTO $dto)
    {
        DB::beginTransaction();
        try {
            $camera = $dto->getCamera();
            $camera->name = $dto->getName();
            $camera->link = $dto->getLink();
            $camera->section_id = $dto->getSectionId();
            $camera->favorite = $dto->getFavorite();
            $camera->update();
        } catch (Exception $exception) {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $camera;
    }
}
