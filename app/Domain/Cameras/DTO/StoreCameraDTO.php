<?php


namespace App\Domain\Cameras\DTO;


class StoreCameraDTO
{
    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $link;

    /**
     * @var int|null
     */
    private ?int $section_id;

    /**
     * @var int
     */
    private int $favorite;

    /**
     * @param array $data
     * @return StoreCameraDTO
     */
    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setName($data['name']);
        $dto->setLink($data['link']);
        $dto->setSectionId($data['section_id']);
        $dto->setFavorite($data['favorite']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return int|null
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * @param int|null $section_id
     */
    public function setSectionId($section_id)
    {
        $this->section_id = $section_id;
    }

    /**
     * @return int
     */
    public function getFavorite(): int
    {
        return $this->favorite;
    }

    /**
     * @param int $favorite
     */
    public function setFavorite(int $favorite): void
    {
        $this->favorite = $favorite;
    }
}
