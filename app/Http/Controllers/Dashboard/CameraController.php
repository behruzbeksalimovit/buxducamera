<?php

namespace App\Http\Controllers\Dashboard;

use App\Domain\Cameras\Models\Camera;
use App\Domain\Cameras\Repositories\CameraRepository;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;

class CameraController extends Controller
{
    /**
     * @var mixed
     */
    private $api_url;

    /**
     * @var mixed
     */
    private $api_port;

    /**
     * @var mixed
     */
    private $api_login;

    /**
     * @var mixed
     */
    private $api_password;

    /**
     *
     */
    public function __construct()
    {
        $this->api_url = env('API_URL');
        $this->api_port = env('API_PORT');
        $this->api_login = env('API_LOGIN');
        $this->api_password = env('API_PASSWORD');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param CameraRepository $cameraRepository
     * @return Factory|View
     * @throws GuzzleException
     */
    public function index(Request $request, CameraRepository $cameraRepository)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://uniwork.buxdu.uz/api/departments.asp');

        $departments = json_decode($response->getBody())->departments;

        if ($request->sub_id != 0) {
            $cameras = Camera::orWhere('section_id', '=', $request->section_id)
                ->where('sub_id', '=', $request->sub_id)
                ->paginate(8);
        } else {
            $cameras = $cameraRepository->getAll();
        }

        return view('dashboard.cameras.filter   ', [
            'sub_nulls' => $departments,
            'cameras' => $cameras,
            'api_url' => $this->api_url,
            'api_port' => $this->api_port
        ]);
    }

    public function indexFilter(Request $request)
    {
        $cameras = Camera::orWhere('section_id', '=', $request->section_id)
            ->where('sub_id', '=', $request->sub_id)
            ->paginate(8);
        return view('dashboard.cameras.index', ['cameras' => $cameras, 'api_url' => $this->api_url, 'api_port' => $this->api_port]);
    }

    public function ajaxGetRahbarId(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments.asp');
        $departments = json_decode($response_sub_id->getBody())->departments;
        $arr_sub_not_null = [];

        for ($i = 0; $i < count($departments); $i++) {
            if ($departments[$i]->id == $request->id) {
                $arr_sub_not_null[$i] = $departments[$i];
            }
        }

        return response([
            'departments' => $arr_sub_not_null
        ], 201);
    }

    public function ajaxGetSubId(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $request->sub_id);
        $departments = json_decode($response_sub_id->getBody())->departments;

        return response([
            'departments' => $departments
        ], 201);
    }

    public function ajaxXonalar(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response_rahbar_id = $client->request('POST', "https://uniwork.buxdu.uz/api/xonalar.asp?bino=" . $request->bino . "&" . "qavat=" . $request->qavat);

        $binolar = json_decode($response_rahbar_id->getBody())->xonalar;

        return response([
            'binolar' => $binolar
        ], 201);
    }

    /**
     * @return Factory|Application|View
     * @throws GuzzleException
     */
    public function create()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://uniwork.buxdu.uz/api/departments.asp');
        $response_bino = $client->request('GET', 'https://uniwork.buxdu.uz/api/binolar.asp');

        $binolar = json_decode($response_bino->getBody())->binolar;
        $departments = json_decode($response->getBody())->departments;

        return view('dashboard.cameras.create', [
            'sub_nulls' => $departments,
            'binolar' => $binolar,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'link' => 'required',
            'sub_id' => 'required',
        ]);

        $client = new \GuzzleHttp\Client();
        $camera = new Camera();
        $name = $request->name;
        $link = $request->link;
        $favorite = $request->favorite ? $request->favorite : 0;
        $bolim_name = $request->bolim_name ? $request->bolim_name : 0;
        $sub_bolim_name = $request->sub_bolim_name ? $request->sub_bolim_name : 0;
        $section_id = $request->section_id ? $request->section_id : 0;
        $sub_id = $request->sub_id;
        $rahbar_id = $request->rahbar_id ? $request->rahbar_id : 0;
        $bino = $request->bino ? $request->bino : 0;
        $qavat = $request->qavat ? $request->qavat : 0;
        $xonalar = $request->xonalar ? $request->xonalar : 0;
        $xona_id = $request->xona_id ? $request->xona_id : 0;
        $uuid = Str::uuid()->toString();

        $data = json_decode("{
              \"name\": \"$name\",
              \"channels\": {
                  \"0\": {
                      \"name\": \"ch1\",
                      \"url\": \"$link\",
                      \"on_demand\": true,
                      \"debug\": false,
                      \"status\": 0
                  }
              }
          }");

        $client->request("POST", "http://$this->api_url:$this->api_port/stream/$uuid/add", [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'auth' => [
                $this->api_login,
                $this->api_password
            ],
            'json' => $data
        ]);
        $camera->name = $name;
        $camera->link = $link;
        $camera->uuid = $uuid;
        $camera->section_id = $section_id;
        $camera->sub_id = $sub_id;
        $camera->rahbar_id = $rahbar_id;
        $camera->bino = $bino;
        $camera->qavat = $qavat;
        $camera->xonalar = $xonalar;
        $camera->favorite = $favorite;
        $camera->rahbarlar = implode(',', $request->rahbarlar);
        $camera->bolim_name = $bolim_name;
        $camera->sub_bolim_name = $sub_bolim_name;
        $camera->xona_id = $xona_id;
        $camera->save();
        return redirect()->route('cameras.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Camera $camera
     * @return Factory|View
     * @throws GuzzleException
     */
    public function edit(Camera $camera)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://uniwork.buxdu.uz/api/departments.asp');
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $camera->sub_id);
        $response_bino = $client->request('GET', 'https://uniwork.buxdu.uz/api/binolar.asp');
        $departments = json_decode($response->getBody())->departments;
        $sub_not_nulls = json_decode($response_sub_id->getBody())->departments;
        $response_binolar = $client->request('POST', 'https://uniwork.buxdu.uz/api/xonalar.asp?bino=' . $camera->bino . '&' . 'qavat=' . $camera->qavat);
        $xonalar = json_decode($response_binolar->getBody())->xonalar;


        $binolar = json_decode($response_bino->getBody())->binolar;

        return view('dashboard.cameras.edit', compact('camera', 'departments', 'sub_not_nulls', 'xonalar','binolar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Camera $camera
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function update(Request $request, Camera $camera)
    {
        $request->validate([
            'name' => 'required',
            'link' => 'required',
            'sub_id' => 'required',
        ]);

        $client = new \GuzzleHttp\Client();
        $cameras = Camera::find($camera->id);
        $name = $request->name;
        $link = $request->link;
        $favorite = $request->favorite ? $request->favorite : 0;
        $bolim_name = $request->bolim_name ? $request->bolim_name : 0;
        $sub_bolim_name = $request->sub_bolim_name ? $request->sub_bolim_name : 0;
        $section_id = $request->section_id ? $request->section_id : 0;
        $sub_id = $request->sub_id ? $request->sub_id : 0;
        $rahbar_id = $request->rahbar_id ? $request->rahbar_id : 0;
        $bino = $request->bino ? $request->bino : 0;
        $qavat = $request->qavat ? $request->qavat : 0;
        $xonalar = $request->xonalar ? $request->xonalar : 0;
        $xona_id = $request->xona_id ? $request->xona_id : 0;
        $data = json_decode("{
              \"name\": \"$name\",
              \"channels\": {
                  \"0\": {
                      \"name\": \"ch1\",
                      \"url\": \"$link\",
                      \"on_demand\": true,
                      \"debug\": false,
                      \"status\": 0
                  }
              }
          }");


        $client->request("POST", "http://$this->api_url:$this->api_port/stream/$camera->uuid/edit", [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
            'auth' => [
                $this->api_login,
                $this->api_password
            ],
            'json' => $data
        ]);

        $cameras->name = $name;
        $cameras->link = $link;
        $cameras->section_id = $sub_id;
        $cameras->favorite = $favorite;
        $cameras->sub_id = $section_id;
        $cameras->rahbar_id = $rahbar_id;
        $cameras->bino = $bino;
        $cameras->qavat = $qavat;
        $cameras->xonalar = $xonalar;
        $cameras->bolim_name = $bolim_name;
        $cameras->sub_bolim_name = $sub_bolim_name;
        $cameras->xona_id = $xona_id;
        $cameras->update();
        return redirect()->route('cameras.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Camera $camera
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function destroy(Camera $camera)
    {
        $client = new \GuzzleHttp\Client();
        $client->request("GET", "http://$this->api_url:$this->api_port/stream/$camera->uuid/delete", [
            'auth' => [
                $this->api_login,
                $this->api_password
            ],
        ]);
        $camera->delete();
        return redirect()->back();
    }

    public function login()
    {
        return view('auth.login');
    }
}
