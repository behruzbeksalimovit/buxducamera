<?php

namespace App\Http\Controllers\Dashboard;

use App\Domain\Cameras\Models\Camera;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CameraApiController extends Controller
{
    /**
     * @var mixed
     */
    private $api_url;

    /**
     * @var mixed
     */
    private $api_port;

    /**
     *
     */
    public function __construct()
    {
        $this->api_url = env('API_URL');
        $this->api_port = env('API_PORT');
    }

    /**
     * @return JsonResponse
     */
    public function getAllCamera()
    {
        $cameras = Camera::select('id','xona_id','name','uuid')->get();

        $array_cameras = array();

        for ($i=0; $i<count($cameras); $i++){
            $array_cameras[$i] = [
                'id' => $cameras[$i]->id,
                'xona_id' => $cameras[$i]->xona_id,
                'nomi' => $cameras[$i]->name,
                'link' => 'http://' . $this->api_url . ':' . $this->api_port . '/stream/' . $cameras[$i]->uuid . '/channel/0/hls/live/index.m3u8'
            ];
        }

        return response()->json([
            'cameras' => $array_cameras
        ],200)->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function camera(Request $request)
    {

        $cameras = Camera::select('id','xona_id','name','uuid')
            ->where('xona_id','=', $request->xona_id)
            ->get();

        $array_camera = array();

        for ($i=0; $i<count($cameras); $i++){
            $array_camera[$i] = [
                'id' => $cameras[$i]->id,
                'xona_id' => $cameras[$i]->xona_id,
                'nomi' => $cameras[$i]->name,
                'link' => 'http://' . $this->api_url . ':' . $this->api_port . '/stream/' . $cameras[$i]->uuid . '/channel/0/hls/live/index.m3u8'
            ];
        }

        return response()->json([
            'data'=> $array_camera,
        ])->setEncodingOptions(JSON_UNESCAPED_SLASHES);
    }
}
