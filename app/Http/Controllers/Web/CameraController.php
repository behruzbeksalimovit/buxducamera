<?php

namespace App\Http\Controllers\Web;

use App\Domain\Cameras\Models\Camera;
use App\Domain\Cameras\Repositories\CameraRepository;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CameraController extends Controller
{
    /**
     * @var mixed
     */
    private $api_url;

    /**
     * @var mixed
     */
    private $api_port;

    /**
     * @var mixed
     */
    private $api_login;

    /**
     * @var mixed
     */
    private $api_password;

    /**
     *
     */
    public function __construct()
    {
        $this->api_url = env('API_URL');
        $this->api_port = env('API_PORT');
        $this->api_login = env('API_LOGIN');
        $this->api_password = env('API_PASSWORD');
    }

    public function index()
    {
        $cameras = Camera::where('rahbar_id', '=', Session::get('id'))
            ->orWhere('rahbarlar', 'LIKE', '%' . Session::get('id') . '%')
            ->paginate(8);
        return view('web.layout.index', ['cameras' => $cameras, 'api_url' => $this->api_url, 'api_port' => $this->api_port]);
    }

    public function ajax(CameraRepository $cameraRepository)
    {
        $cameras = $cameraRepository->all();

        return response([
            'cameras' => $cameras,
            'api_url' => $this->api_url,
            'api_port' => $this->api_port
        ], 200);
    }

    /**
     * @return Application|Factory|View
     */
    public function loginPage()
    {
        return view('web.login.login');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws GuzzleException
     */
    public function login(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $login = $request->login;
        $parol = $request->parol;
        $avtorizatsiya = $request->avtorizatsiya;

        $response = $client->request('POST', 'https://uniwork.buxdu.uz/api/login.asp', [
            'form_params' => [
                'login' => $login,
                'parol' => $parol,
                'avtorizatsiya' => $avtorizatsiya,
            ]
        ]);
        $result_error = json_decode($response->getBody());
        if (isset($result_error->code) == false) {
            $result = json_decode($response->getBody())->id;
            $result_fio = json_decode($response->getBody())->fio;
            $result_department = json_decode($response->getBody())->department;
            Session::put('id', $result);
            Session::put('fio', $result_fio);
            Session::put('department', $result_department);

//            if (Session::get('id') == env('REKTOR_ID')) {
//                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
//                return redirect()->route('rector');
//            } else {
//                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
//                return redirect()->route('web');
//            }

            if (Session::get('id') == env('REKTOR_ID')) {
                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
                return redirect()->route('rector');
            } elseif(Session::get('department') != 'null') {
                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
                return redirect()->route('web');
            }else{
                return redirect()->route('user.loginPage');
            }

//            if (Session::get('id') == 2555) {
//                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
//                return redirect()->route('rector');
//            } elseif(Session::get('department') != 'null') {
//                alert()->success("Camera", "Siz tizimga muvaffaqiyatli kirdingiz!");
//                return redirect()->route('web');
//            }else{
//                return redirect()->route('user.loginPage');
//            }

        } else {
            $err = $result_error->error->message;
            alert()->error("Login yoki parol xato!", "$err");
            return redirect()->back();
        }
    }

    //    rektor

    public function ajaxGetSubId(Request $request)
    {
        $client = new \GuzzleHttp\Client();
        $response_sub_id = $client->request('POST', 'https://uniwork.buxdu.uz/api/departments_filter.asp?sub=' . $request->sub_id);
        $departments = json_decode($response_sub_id->getBody())->departments;

        return response([
            'departments' => $departments
        ], 201);
    }

    /**
     * @return Application|Factory|View
     */
    public function getSection()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', 'https://uniwork.buxdu.uz/api/departments.asp');

        $departments = json_decode($response->getBody())->departments;
        return view('web.rektor.rektor', [
            'sub_nulls' => $departments,
        ]);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View
     */
    public function rektor(Request $request)
    {
        $rektor_id = env('REKTOR_ID');
        $cameras = Camera::where('rahbarlar', 'LIKE', '%' . $rektor_id . '%')
            ->orWhere('section_id', '=', $request->section_id)
            ->orWhere('sub_id', '=', $request->sub_id)
            ->get();
        return view('web.rektor.viewCamera', ['cameras' => $cameras, 'api_url' => $this->api_url, 'api_port' => $this->api_port]);
    }
//    rektor


//user aall camera favorite show
    /**
     * @return Application|Factory|View
     */
    public function userShowAllCamera()
    {
        $cameras = Camera::select('bolim_name', 'sub_id')
            ->where('favorite', '=', 1)
            ->groupBy('sub_id', 'bolim_name')
            ->paginate(10);
        return view('web.user.camera', ['cameras' => $cameras, 'api_url' => $this->api_url, 'api_port' => $this->api_port]);
    }

    /**
     * @param $sub_id
     * @return Application|Factory|View
     */
    public function userSubShowAllCamera($sub_id)
    {
        $cameras = Camera::where('favorite', '=', 1)
            ->where('sub_id','=', $sub_id)
            ->paginate(10);

        return view('web.user.sub_camera',['cameras' => $cameras,'api_url' => $this->api_url, 'api_port' => $this->api_port]);
    }
//user aall camera favorite show
}
