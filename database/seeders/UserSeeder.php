<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'login' => config('settings.admin_login'),
            'password' => Hash::make(config('settings.admin_password')),
            'name' => config('settings.admin_name'),
            'role' => 1
        ]);
    }
}
