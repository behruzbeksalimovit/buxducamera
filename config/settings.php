<?php

return [
    'admin_name' => env('ADMIN_NAME', 'Admin'),
    'admin_login' => env('ADMIN_LOGIN', 'Admin'),
    'admin_password' => env('ADMIN_PASSWORD', 'secret'),
];
