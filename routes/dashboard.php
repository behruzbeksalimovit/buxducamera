<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Dashboard\CameraController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Route::group(['prefix' => 'admin'], function (){
    Route::get('/', [CameraController::class,'login']);
    Route::post('login', [LoginController::class,'login'])->name('admin.login');
});

Route::group(['prefix' => 'dashboard','middleware' => ['auth','admin','preventBack']], function (){
    Route::get('/main', function () {
        return view('dashboard.layout.index');
    })->name('dashboard');
   Route::resource('/cameras','\App\Http\Controllers\Dashboard\CameraController');
   Route::post('/ajax/rahbarId',[CameraController::class, 'ajaxGetRahbarId'])->name('ajax.getRahbarId');
   Route::post('/ajax/getId',[CameraController::class, 'ajaxGetSubId'])->name('ajax.getSubId');
   Route::post('/ajax/getBinolar',[CameraController::class, 'ajaxXonalar'])->name('ajax.xonalar');
   Route::post('/cameras/filter',[CameraController::class, 'indexFilter'])->name('camera.filter');
//   rektor

//   rektor
});
Route::middleware(['middleware'=>'preventBack'])->group(function (){
    Auth::routes();
});

