<?php

use App\Http\Controllers\Web\CameraController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes

|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

Route::get('/kirish',[CameraController::class,'loginPage'])->name('user.loginPage');
Route::post('/user/login',[CameraController::class,'login'])->name('user.login');

Route::get('/',[CameraController::class,'userShowAllCamera'])->name('user.camera');
Route::get('/camera/{sub_id}',[CameraController::class,'userSubShowAllCamera'])->name('user.cameraSub');

Route::group(['prefix' => 'web', 'middleware' => ['user','preventBack']], function (){
    Route::get('/index',[\App\Http\Controllers\Web\CameraController::class, 'index'])->name('web');
    Route::post('/ajax', [\App\Http\Controllers\Web\CameraController::class, 'ajax'])->name('camera.ajax');

////rektor
//    Route::post('/ajax/getId/rektor',[CameraController::class, 'ajaxGetSubId'])->name('ajax.getSubIdRektor');
//    Route::get('/rektor/section', [CameraController::class,'getSection'])->name('camera.rektorSection');
//    Route::post('/rektor', [CameraController::class,'rektor'])->name('camera.rektor');
////rektor
});


//Route::get('/', [\App\Http\Controllers\Web\CameraController::class, 'index'])->name('web.index');


