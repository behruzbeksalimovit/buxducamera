<?php

use App\Http\Controllers\Web\CameraController;
use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'rector', 'middleware' => ['rector','preventBack']], function (){
    Route::get('/',[\App\Http\Controllers\Web\CameraController::class, 'index'])->name('rector');
//    Route::post('/ajax', [\App\Http\Controllers\Web\CameraController::class, 'ajax'])->name('camera.ajax');

    //rektor
    Route::post('/ajax/getId/rektor',[CameraController::class, 'ajaxGetSubId'])->name('ajax.getSubIdRektor');
    Route::get('/rektor/section', [CameraController::class,'getSection'])->name('camera.rektorSection');
    Route::post('/rektor', [CameraController::class,'rektor'])->name('camera.rektor');
    //rektor
});
