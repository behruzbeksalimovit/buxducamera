<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['middleware' => 'camera_api'], function (){
    Route::get('camera/all',[\App\Http\Controllers\Dashboard\CameraApiController::class, 'getAllCamera'])->name('cameras.all');
    Route::get('camera',[\App\Http\Controllers\Dashboard\CameraApiController::class, 'camera'])->name('camera.link');
});
