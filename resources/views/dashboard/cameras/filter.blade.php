@extends('dashboard.layout.master')
@section('content')
    <div class="row pt-4">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Kameralar</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="get" action="{{ route('cameras.index') }}" enctype="multipart/form-data">
{{--                    @csrf--}}
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="parent">Bo'lim</label>
                                    <select name="sub_id" id="parent" class="form-control section select2bs4" style="width: 100%; height: 100%">
                                        <option hidden value="0">Bo'limni tanlang!</option>
                                        @foreach($sub_nulls as $section)
                                            <option data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" value="{{ $section->id }}">{{ $section->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group sec">
                                    <label for="section_id">Sub bo'limlar</label>
                                    <select name="section_id" id="section_id" class="form-control section_sub">
                                        <option hidden selected value="0">Sub bo'limni tanlang!</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Izlash</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row pt-2">
        @foreach($cameras as $camera)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a href="{{ route('cameras.edit',$camera) }}">{{ $camera->name }}</a>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-2">

                        <video id="hls-example" class="video-js vjs-default-skin" width="380" height="300" controls autoplay>
                            <source  type="application/x-mpegURL" src="http://{{ $api_url }}:{{ $api_port }}/stream/{{ $camera->uuid }}/channel/0/hls/live/index.m3u8">
                        </video>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer p-2">
                        <form class="d-inline-block" action="{{ route('cameras.destroy', $camera) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger show_confirm" data-toggle="tooltip" type="submit">O'chirish</button>
                        </form>
                        <a class="btn btn-primary d-inline-block" href="{{ route('cameras.edit',$camera) }}">Tahrirlash</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="d-flex align-items-center justify-content-center" style="margin-top: 20px;">
        {{ $cameras->links() }}
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
    <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
    <script type="text/javascript">
        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Siz haqiqatdan ushbu kamerani o'chirmoqchimisiz?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });
    </script>
    <script>
        let camera = document.querySelectorAll('#hls-example');
        camera.forEach(camera=>{
            var player = videojs(camera);
            player.play()
        });
    </script>
    <script>
        $(document).ready(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".section").change(function (){
                $('.bolim-name').remove();
                $('.sub_bolim_name').remove();
                $(".bolim").remove();
                $('.section_sub').html('');
                let sub_id = $(this).val();
                let options = $('.section option');
                for(let i=0; i<options.length; i++)
                {
                    if(sub_id === options[i].getAttribute('value')){
                        rahbar_bolim_id = options[i].getAttribute('data-bolim-rahbar-id');
                        bolim_name = options[i].getAttribute('data-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getSubId') }}',
                    method: 'POST',
                    data: {
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-sub-bolim-name='"+department.name+"' data-sub-rahbar-id='"+department.rahbar_id+"' data-section='"+ department.sub_id+"' value='"+department.id+"'>"+ (department.name) +"</option>";
                        });
                        formoption += "<option selected value='0'>Bo'limni tanlang</option>";
                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none bolim" value="${rahbar_bolim_id}"/>
                            <input name="bolim_name" class="d-none bolim-name" value="${bolim_name}"/>
                        `);

                        $(".section_sub").append(formoption);
                    },
                    // error: function (e) {
                    //     console.log(e);
                    // }
                });
            });

            $(".section_sub").change(function (){
                $('.sub_bolim_name').remove();
                $(".sub__bolim").remove();
                $('.rahbar_id').html('');
                let id = $(this).val();
                let sub_id = $(this).attr('data-section');
                let options = $('.section_sub option');
                for(let i=0; i<options.length; i++)
                {
                    if(id === options[i].getAttribute('value')){
                        rahbar_sub_id = options[i].getAttribute('data-sub-rahbar-id');
                        sub_bolim_name = options[i].getAttribute('data-sub-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getRahbarId') }}',
                    method: 'POST',
                    data: {
                        id: id,
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-section-rahbar='"+ department.sub_id+"' value='"+department.rahbar_id+"'>"+ department.rahbar +"</option>";
                        });
                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none sub__bolim" value="${rahbar_sub_id}"/>
                            <input name="sub_bolim_name" class="d-none sub_bolim_name" value="${sub_bolim_name}"/>
                        `);
                        $(".rahbar_id").append(formoption);
                    },

                    // error: function (e) {
                    //     console.log(e);
                    // }
                });
            });

            $('select[name="sub_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='section_id']").val("");
                $(".sec").removeClass('d-none');

                $('select[name="section_id"] option').each(function() {
                    if ($(this).attr('data-section') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

            $('select[name="section_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='rahbar_id']").val("");
                $(".rahbar").removeClass('d-none');

                $('select[name="rahbar_id"] option').each(function() {
                    if ($(this).attr('data-section-rahbar') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });
        });
    </script>
@endpush
