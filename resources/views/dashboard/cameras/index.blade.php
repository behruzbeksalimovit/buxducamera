@extends('dashboard.layout.master')
@section('content')
    <div class="row pt-2">
        @foreach($cameras as $camera)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a href="{{ route('cameras.edit',$camera) }}">{{ $camera->name }}</a>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-2">

                        <video id="hls-example" class="video-js vjs-default-skin" width="380" height="300" controls autoplay>
                            <source  type="application/x-mpegURL" src="http://{{ $api_url }}:{{ $api_port }}/stream/{{ $camera->uuid }}/channel/0/hls/live/index.m3u8">
                        </video>
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer p-2">
                        <form class="d-inline-block" action="{{ route('cameras.destroy', $camera) }}" method="post">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger show_confirm" data-toggle="tooltip" type="submit">O'chirish</button>
                        </form>
                        <a class="btn btn-primary d-inline-block" href="{{ route('cameras.edit',$camera) }}">Tahrirlash</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
{{--        <div class="row">--}}
            <div class="d-flex align-items-center justify-content-center" style="margin-top: 20px;">
            {{ $cameras->links() }}
            </div>
{{--        </div>--}}
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
    <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Siz haqiqatdan ushbu kamerani o'chirmoqchimisiz?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>

    <script>
        let camera = document.querySelectorAll('#hls-example');
        camera.forEach(camera=>{
            var player = videojs(camera);
            player.play()
        });
    </script>
@endpush
