@extends('dashboard.layout.master')
@section('content')
    <div class="row pt-4">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Kamera o'zgartirish</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('cameras.update', $camera) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="row mb-2">
                            <div class="col-md-4">
                                <label for="parent">Bo'lim</label>
                                <select name="section_id" id="parent" class="form-control section">
                                    @foreach($departments as $section)
                                        @if($section->id == $camera->sub_id)
                                            <option data-sub_id="{{ $section->id }}" data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" selected value="{{ $section->id }}">{{ $section->name }}</option>
                                        @endif
                                        @if($section->id != $camera->sub_id)
                                            <option data-sub_id="{{ $section->id }}" style="display: block" data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" value="{{ $section->id }}">{{ $section->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 sec">
                                <label for="sub_id">Sub bo'limlar</label>
                                <select name="sub_id" id="sub_id" class="form-control section_sub">
                                    @if($camera->section_id == 0)
                                        <option value="0" selected>Bo'limni tanlang</option>
                                    @endif
                                    @if($camera->section_id != 0)
                                        <option value="0">Bo'limni tanlang</option>
                                    @endif
                                    @foreach($sub_not_nulls as $section)

                                        @if($camera->section_id == $section->id)
                                            <option data-sub-rahbar-id="{{ $section->rahbar_id }}" data-section="{{ $section->sub_id }}" data-sub-bolim-name="{{ $section->name }}" selected value="{{ $section->id }}" >{{ $section->name }}</option>
                                        @endif
                                        @if($camera->section_id != $section->id)
                                            <option data-sub-rahbar-id="{{ $section->rahbar_id }}" data-section="{{ $section->sub_id }}" data-sub-bolim-name="{{ $section->name }}" value="{{ $section->id }}" >{{ $section->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-md-4 rahbar">
                                <label for="rahbar_id">Rahbarlar</label>
                                <select name="rahbar_id" id="rahbar_id" class="form-control rahbar_id">
{{--                                    @foreach($sub_not_nulls as $section)--}}
{{--                                        @if($camera->rahbar_id == $section->rahbar_id)--}}
                                    @foreach($sub_not_nulls as $section)
                                        @if($camera->rahbar_id == $section->rahbar_id)
                                            <option data-section-rahbar="{{ $section->sub_id }}" selected value="{{ $section->rahbar_id }}">{{ $section->rahbar }}</option>
                                        @endif
                                    @endforeach

{{--                                    <option selected value="{{ $camera->rahbar_id }}">{{ $camera->rahbar }}</option>--}}
{{--                                        @elseif($camera->rahbar_id != $section->rahbar_id)--}}
{{--                                            <option value="{{ $camera->rahbar_id }}">{{ $section->rahbar }}</option>--}}
{{--                                        @endif--}}
{{--                                    @endforeach--}}
                                </select>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-4">
                                <label for="bino">Binolar</label>
                                <select name="bino" id="bino" class="form-control">
{{--                                    @for($i=1;$i<=3; $i++)--}}
{{--                                        @if($camera->bino == $i)--}}
{{--                                            <option selected="selected" value="{{ $i }}">{{ $i }}</option>--}}
{{--                                        @endif--}}
{{--                                        @if($camera->bino != $i)--}}
{{--                                            <option value="{{ $i }}">{{ $i }}</option>--}}
{{--                                        @endif--}}
{{--                                    @endfor--}}
                                    @foreach($binolar as $bino)
                                        @if($camera->bino == $bino->id)
                                            <option selected value="{{ $bino->id }}">{{ $bino->name }}</option>
                                        @endif
                                        @if($camera->bino != $bino->id)
                                            <option value="{{ $bino->id }}">{{ $bino->name }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="qavat">Qavatlar</label>
                                <select name="qavat" id="qavat" class="form-control">
                                    <option id="default">Select floor</option>
                                    @for($i=1;$i<=6; $i++)
                                        @if($camera->qavat == $i)
                                            <option selected="selected" value="{{ $i }}">{{ $i }}</option>
                                        @endif
                                        @if($camera->qavat != $i)
                                            <option value="{{ $i }}">{{ $i }}</option>
                                        @endif
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="xonalar">Xonalar</label>
{{--                                <select name="xonalar" id="xonalar" class="form-control xonalar">--}}
{{--                                    <option value="{{ $camera->xonalar }}">{{ $camera->xonalar }}</option>--}}
{{--                                </select>--}}
                                <select name="xonalar" id="xonalar" class="form-control xonalar">
                                    <option id="default_xonalar">Select room</option>
                                    @foreach($xonalar as $xona)
                                        @if($camera->xonalar == $xona->xona_nomi)
                                            <option data-xona-id="{{ $xona->id }}" selected value="{{ $xona->xona_nomi }}">{{ $xona->xona_nomi }}</option>
                                        @elseif($camera->xonalar != $xona->xona_nomi)
                                            <option data-xona-id="{{ $xona->id }}" value="{{ $xona->xona_nomi }}">{{ $xona->xona_nomi }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name">Kamera nomi</label>
                            <input value="{{ $camera->name }}" type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Kamera nomini kiriting">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="link">Kamera linki</label>
                            <input value="{{ $camera->link }}" type="text" name="link" class="form-control @error('link') is-invalid @enderror" id="link" placeholder="Kamera linkini kiriting">
                            @error('link')
                            <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input name="favorite" value="1" class="custom-control-input custom-control-input-success" type="checkbox" id="customCheckbox4" @if($camera->favorite == 1) checked="" @endif>
                            <label for="customCheckbox4" class="custom-control-label">Favorites</label>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div id="add_input">
                        <input type="text" name="rahbarlar[]" id="rahbarlar" class="d-none" value="2555">
                    </div>
                    <div class="xona_id d-none">
                        <input value="{{$camera->xona_id}}" type="hidden" name="xona_id">
                    </div>

                    <div class="bolim d-none">
                        <input name="bolim_name" type="hidden"  class="d-none" value="{{$camera->bolim_name}}"/>
                        <input name="rahbar_id"  type="hidden" class="d-none" value="{{$camera->rahbar_id}}"/>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Saqlash</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            var rahbar_bolim_id = '';
            var bolim_name = '';
            var sub_bolim_name = '';
            var rahbar_sub_id = '';
            var rahbar_sub_id_2 = '';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".section").change(function (){
                $('.bolim-name').remove();
                $('.sub_bolim_name').remove();
                $(".bolim").remove();
                $('.section_sub').html('');
                let sub_id = $(this).val();
                let options = $('.section option');
                for(let i=0; i<options.length; i++)
                {
                    if(sub_id === options[i].getAttribute('value')){
                        rahbar_bolim_id = options[i].getAttribute('data-bolim-rahbar-id');
                        bolim_name = options[i].getAttribute('data-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getSubId') }}',
                    method: 'POST',
                    data: {
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option value='0' selected>Bo'limni tanlang</option>";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-sub-bolim-name='"+department.name+"' data-sub-rahbar-id='"+department.rahbar_id+"' data-section='"+ department.sub_id+"' value='"+department.id+"'>"+ (department.name) +"</option>";
                        });

                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none bolim" value="${rahbar_bolim_id}"/>
                            <input name="bolim_name" class="d-none bolim-name" value="${bolim_name}"/>
                        `);
                        $(".section_sub").append(formoption);
                    },
                });
            });

            $(".section_sub").change(function (){
                $('option', this).removeAttr('selected');

                $('.sub_bolim_name').remove();
                $(".sub__bolim").remove();
                $('.rahbar_id').html('');
                let id = $(this).val();
                let sub_id = $(this).attr('data-section');
                let options = $('.section_sub option');
                for(let i=0; i<options.length; i++)
                {
                    if(id === options[i].getAttribute('value')){
                        rahbar_sub_id = options[i].getAttribute('data-sub-rahbar-id');
                        sub_bolim_name = options[i].getAttribute('data-sub-bolim-name');
                    }
                }

                $.ajax({
                    url: '{{ route('ajax.getRahbarId') }}',
                    method: 'POST',
                    data: {
                        id: id,
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-section-rahbar='"+ department.sub_id+"' value='"+department.id+"'>"+ department.rahbar +"</option>";
                        });
                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none sub__bolim" value="${rahbar_sub_id}"/>
                            <input name="sub_bolim_name" class="d-none sub_bolim_name" value="${sub_bolim_name}"/>
                        `);
                        $(".rahbar_id").append(formoption);
                    },
                });
            });

            $('select[name="sub_id"]').change(function() {
                var parent = $(this).val();
                var section_id = $(this).attr('data-section');

                // $("select[name='section_id']").val("");
                $(".sec").removeClass('d-none');

                // $('select[name="section_id"] option').each(function() {
                //     if ($(this).attr('data-sub_id') == parent) {
                //         $('option', this).attr('selected','selected');
                //     }
                //         $(this).show();
                //     // else {
                //     //     $(this).hide();
                //     // }
                // });
            });

            $('select[name="section_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='rahbar_id']").val("");
                $(".rahbar").removeClass('d-none');

                $('select[name="rahbar_id"] option').each(function() {
                    if ($(this).attr('data-section-rahbar') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

            var bino=0;
            $("#bino").change(function (){
                $("#xonalar").html('');
                $("#bino option").removeAttr('selected');
                $("#qavat option#default").attr('selected','selected');
                bino = $(this).val();
            });

            $("#qavat").change(function (){
                $("#qavat option").removeAttr('selected');
                var a=$("#bino option:selected").val();
                $("#xonalar").html('');
                // $("#qavat").val("0");
                let qavat = $(this).val();
                $.ajax({
                    url: '{{ route('ajax.xonalar') }}',
                    method: 'POST',
                    data: {
                        bino: ((bino ==0 ) ? a : bino),
                        qavat: qavat,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option selected value='0'>Xonalarni tanlang</option>";
                        $.each(response.binolar, function(index, xona) {
                            formoption += "<option data-xona-id="+xona.id+" value='"+xona.xona_nomi+"'>"+ xona.xona_nomi +"</option>";
                        });
                        $("#xonalar").append(formoption);
                    },
                });
            });

            {{--$("#qavat").change(function (){--}}
            {{--    $(".xonalar").html('');--}}
            {{--    let qavat = $(this).val();--}}
            {{--    let bino = $("#bino").val();--}}
            {{--    $.ajax({--}}
            {{--        url: '{{ route('ajax.xonalar') }}',--}}
            {{--        method: 'POST',--}}
            {{--        data: {--}}
            {{--            bino: bino,--}}
            {{--            qavat: qavat,--}}
            {{--        },--}}
            {{--        success: function (response) {--}}
            {{--            var formoption = "";--}}
            {{--            formoption += "<option selected value='0'>Xonalarni tanlang</option>";--}}
            {{--            $.each(response.binolar, function(index, xona) {--}}
            {{--                formoption += "<option data-xona-id="+xona.id+" value='"+xona.xona_nomi+"'>"+ xona.xona_nomi +"</option>";--}}
            {{--            });--}}
            {{--            $("#xonalar").append(formoption);--}}
            {{--        },--}}
            {{--        error:function (e){--}}
            {{--            console.log(e);--}}
            {{--        }--}}
            {{--    });--}}
            {{--});--}}

            $("#xonalar").change(function (){
                $('.xona_id').html('');
                let xona_id = $('option:selected', this).attr('data-xona-id');
                $('.xona_id').append(`
                    <input name="xona_id" value="${xona_id}" type="hidden">
                `);
            });

            var bolim_name = '';
            var rahbar_id = '';
            var rahbarlar_massiv_id = '';
            $("#sub_id").change(function (){
                $(".bolim").html('');
                var sub_id = $(this).val();
                var options = $("#sub_id option");
                var section = $(".section").find('option:selected').attr('data-sub_id');
                var section_name = $(".section").find('option:selected').attr('data-bolim-name');
                var section_rahbar_id = $(".section").find('option:selected').attr('data-bolim-rahbar-id');
                for(let i=0;i<options.length; i++){
                    if(sub_id === options[i].getAttribute('value')){
                        rahbar_id = options[i].getAttribute('data-sub-rahbar-id');
                    }
                    if(section === options[i].getAttribute('data-section')){
                        bolim_name = section_name;
                        rahbarlar_massiv_id = section_rahbar_id;
                    }
                }
                $(".bolim").append(`
                    <input name="bolim_name" class="d-none" value="${bolim_name}"/>
                    <input name="rahbarlar[]" class="d-none sub__bolim" value="${rahbarlar_massiv_id}"/>
                    <input name="rahbar_id" class="d-none" value="${rahbar_id}"/>
                `);
            });

        });
    </script>
@endpush

