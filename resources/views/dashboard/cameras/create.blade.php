@extends('dashboard.layout.master')
@section('content')
    <div class="row pt-4">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Kamera qo'shish</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('cameras.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label for="parent">Bo'lim</label>
                            <select required="required" name="sub_id" id="parent" class="form-control section select2bs4" style="width: 100%; height: 100%">
                                <option value="">Bo'limni tanlang!</option>
                                @foreach($sub_nulls as $section)
                                    <option data-bolim-name="{{ $section->name }}" data-bolim-rahbar-id="{{ $section->rahbar_id }}" value="{{ $section->id }}">{{ $section->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group sec">
                            <label for="section_id">Sub bo'limlar</label>
                            <select name="section_id" id="section_id" class="form-control section_sub">
                                <option hidden selected value="0">Sub bo'limni tanlang!</option>
                            </select>
                        </div>

                        <div class="form-group rahbar">
                            <label for="rahbar_id">Rahbarlar</label>
                            <select name="rahbar_id" id="rahbar_id" class="form-control rahbar_id">
                                <option hidden selected value="0">Rahbarni tanlang!</option>
                            </select>
                        </div>

                        <div class="row mb-2">
                            <div class="col-md-4">
                                <label for="bino">Binolar</label>
                                <select name="bino" id="bino" class="form-control">
                                    <option value="0" selected>Tanlang</option>
{{--                                    <option value="1">1</option>--}}
{{--                                    <option value="2">2</option>--}}
{{--                                    <option value="3">3</option>--}}
                                    @foreach($binolar as $bino)
                                        <option value="{{ $bino->id }}">{{ $bino->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="qavat">Qavatlar</label>
                                <select name="qavat" id="qavat" class="form-control">
                                    <option value="0" selected>Tanlang</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="xonalar">Xonalar</label>
                                <select name="xonalar" id="xonalar" class="form-control xonalar">
                                    <option value="0" selected>Tanlang</option>
                                </select>
                                @error('xonalar')
                                <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="name">Kamera nomi</label>
                                    <input value="{{ old('name') }}" type="text" name="name"
                                           class="form-control @error('name') is-invalid @enderror" id="name"
                                           placeholder="Kamera nomini kiriting">
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="link">Kamera linki</label>
                                    <input value="{{ old('link') }}" type="text" name="link"
                                           class="form-control @error('link') is-invalid @enderror" id="link"
                                           placeholder="Kamera linkini kiriting">
                                    @error('link')
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <input name="favorite" value="1" class="custom-control-input custom-control-input-success" type="checkbox" id="customCheckbox4" checked="">
                            <label for="customCheckbox4" class="custom-control-label">Избранное</label>
                        </div>
                    </div>
                    <!-- /.card-body -->
                    <div id="add_input">
                        <input type="text" name="rahbarlar[]" id="rahbarlar" class="d-none" value="2555">
                    </div>
                    <div class="xona_id d-none"></div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Qo'shish</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            var rahbar_bolim_id = '';
            var bolim_name = '';
            var sub_bolim_name = '';
            var rahbar_sub_id = '';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".section").change(function (){
                $('.bolim-name').remove();
                $('.sub_bolim_name').remove();
                $(".bolim").remove();
                $('.section_sub').html('');
                let sub_id = $(this).val();
                let options = $('.section option');
                for(let i=0; i<options.length; i++)
                {
                    if(sub_id === options[i].getAttribute('value')){
                        rahbar_bolim_id = options[i].getAttribute('data-bolim-rahbar-id');
                        bolim_name = options[i].getAttribute('data-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getSubId') }}',
                    method: 'POST',
                    data: {
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option selected value='0'>Bo'limni tanlang</option>";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-sub-bolim-name='"+department.name+"' data-sub-rahbar-id='"+department.rahbar_id+"' data-section='"+ department.sub_id+"' value='"+department.id+"'>"+ (department.name) +"</option>";
                        });

                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none bolim" value="${rahbar_bolim_id}"/>
                            <input name="bolim_name" class="d-none bolim-name" value="${bolim_name}"/>
                        `);

                        $(".section_sub").append(formoption);
                    },
                    // error: function (e) {
                    //     console.log(e);
                    // }
                });
            });

            $(".section_sub").change(function (){
                $('.sub_bolim_name').remove();
                $(".sub__bolim").remove();
                $('.rahbar_id').html('');
                let id = $(this).val();
                let sub_id = $(this).attr('data-section');
                let options = $('.section_sub option');
                for(let i=0; i<options.length; i++)
                {
                    if(id === options[i].getAttribute('value')){
                        rahbar_sub_id = options[i].getAttribute('data-sub-rahbar-id');
                        sub_bolim_name = options[i].getAttribute('data-sub-bolim-name');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getRahbarId') }}',
                    method: 'POST',
                    data: {
                        id: id,
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        console.log(response.departments)
                        var formoption = "";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-section-rahbar='"+ department.sub_id+"' value='"+department.rahbar_id+"'>"+ department.rahbar +"</option>";
                        });
                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none sub__bolim" value="${rahbar_sub_id}"/>
                            <input name="sub_bolim_name" class="d-none sub_bolim_name" value="${sub_bolim_name}"/>
                        `);
                        $(".rahbar_id").append(formoption);
                    },

                    // error: function (e) {
                    //     console.log(e);
                    // }
                });
            });

            $('select[name="sub_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='section_id']").val("");
                $(".sec").removeClass('d-none');

                $('select[name="section_id"] option').each(function() {
                    if ($(this).attr('data-section') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

            $('select[name="section_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='rahbar_id']").val("");
                $(".rahbar").removeClass('d-none');

                $('select[name="rahbar_id"] option').each(function() {
                    if ($(this).attr('data-section-rahbar') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });
            var bino=0;
            $("#bino").change(function (){
                $("#xonalar").html('');
                bino = $(this).val();
                $("#qavat").val("0");
            });
            $("#qavat").change(function (){
                $("#xonalar").html('');
                let qavat = $(this).val();
                $.ajax({
                    url: '{{ route('ajax.xonalar') }}',
                    method: 'POST',
                    data: {
                        bino: bino,
                        qavat: qavat,
                    },
                    success: function (response) {
                        var formoption = "";
                        formoption += "<option selected value='0'>Xonalarni tanlang</option>";
                        $.each(response.binolar, function(index, xona) {
                            formoption += "<option data-xona-id="+xona.id+" value='"+xona.xona_nomi+"'>"+ xona.xona_nomi +"</option>";
                        });
                        $("#xonalar").append(formoption);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            {{--$("#qavat").change(function (){--}}
            {{--    $("#xonalar").html('');--}}
            {{--    // $("#bino").html('');--}}
            {{--    let bino = $(this).val();--}}
            {{--    $("#bino").change(function (){--}}
            {{--        $("#xonalar").html('');--}}
            {{--        let qavat = $(this).val();--}}
            {{--        $.ajax({--}}
            {{--            url: '{{ route('ajax.xonalar') }}',--}}
            {{--            method: 'POST',--}}
            {{--            data: {--}}
            {{--                bino: bino,--}}
            {{--                qavat: qavat,--}}
            {{--            },--}}
            {{--            success: function (response) {--}}
            {{--                var formoption = "";--}}
            {{--                formoption += "<option selected value='0'>Xonalarni tanlang</option>";--}}
            {{--                $.each(response.binolar, function(index, xona) {--}}
            {{--                    formoption += "<option data-xona-id="+xona.id+" value='"+xona.id+"'>"+ xona.xona_nomi +"</option>";--}}
            {{--                });--}}

            {{--                $("#xonalar").append(formoption);--}}
            {{--                formoption = '';--}}

            {{--            },--}}
            {{--            error: function (e) {--}}
            {{--                console.log(e);--}}
            {{--            }--}}
            {{--        });--}}
            {{--    });--}}
            {{--});--}}
            $("#xonalar").change(function (){
                $('.xona_id').html('');
                let xona_id = $('option:selected', this).attr('data-xona-id');
                $('.xona_id').append(`
                    <input name="xona_id" value="${xona_id}" type="hidden">
                `);
            });

        });
    </script>
@endpush

