<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('dashboard') }}" class="brand-link">
        <img src="{{ asset('dashboard/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">BuxDU Camera</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
{{--                <li class="nav-item {{ (request()->is('dashboard/faculties/create') || request()->is('dashboard/faculties')) ? 'menu-is-opening menu-open' : '' }}">--}}
{{--                    <a href="#" class="nav-link {{ (request()->is('dashboard/faculties/create') || request()->is('dashboard/faculties')) ? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-th"></i>--}}
{{--                        <p>--}}
{{--                            Fakultetlar--}}
{{--                            <i class="right fas fa-angle-left"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview {{ (request()->is('dashboard/faculties/create') || request()->is('dashboard/faculties')) ? 'd-block' : '' }}">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('faculties.create') }}" class="nav-link {{ request()->is('dashboard/faculties/create') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Qo'shish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('faculties.index') }}" class="nav-link {{ request()->is('dashboard/faculties') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Ko'rish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="nav-item {{ (request()->is('dashboard/sections/create') || request()->is('dashboard/sections')) ? 'menu-is-opening menu-open' : '' }}">--}}
{{--                    <a href="#" class="nav-link {{ (request()->is('dashboard/sections/create') || request()->is('dashboard/sections')) ? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fa fa-chart-bar"></i>--}}
{{--                        <p>--}}
{{--                            Bo'limlar--}}
{{--                            <i class="right fas fa-angle-left"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview {{ (request()->is('dashboard/sections/create') || request()->is('dashboard/sections')) ? 'd-block' : '' }}">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('sections.create') }}" class="nav-link {{ request()->is('dashboard/sections/create') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Qo'shish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('sections.index') }}" class="nav-link {{ request()->is('dashboard/sections') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Ko'rish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li class="nav-item {{ (request()->is('dashboard/accommondations/create') || request()->is('dashboard/accommondations')) ? 'menu-is-opening menu-open' : '' }}">--}}
{{--                    <a href="#" class="nav-link {{ (request()->is('dashboard/accommondations/create') || request()->is('dashboard/accommondations')) ? 'active' : '' }}">--}}
{{--                        <i class="nav-icon fas fa-bed"></i>--}}
{{--                        <p>--}}
{{--                            Yotoqxonalar--}}
{{--                            <i class="right fas fa-angle-left"></i>--}}
{{--                        </p>--}}
{{--                    </a>--}}
{{--                    <ul class="nav nav-treeview {{ (request()->is('dashboard/accommondations/create') || request()->is('dashboard/accommondations')) ? 'd-block' : '' }}">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('accommondations.create') }}" class="nav-link {{ request()->is('dashboard/accommondations/create') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Qo'shish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a href="{{ route('accommondations.index') }}" class="nav-link {{ request()->is('dashboard/accommondations') ? 'active' : '' }}">--}}
{{--                                <i class="far fa-circle nav-icon"></i>--}}
{{--                                <p>Ko'rish</p>--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
                <li class="nav-item {{ (request()->is('dashboard/cameras/create') || request()->is('dashboard/cameras')) ? 'menu-is-opening menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('dashboard/cameras/create') || request()->is('dashboard/cameras')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-camera"></i>
                        <p>
                            Kameralar
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview {{ (request()->is('dashboard/cameras/create') || request()->is('dashboard/cameras/filter') || request()->is('dashboard/cameras')) ? 'd-block' : '' }}">
                        <li class="nav-item">
                            <a href="{{ route('cameras.create') }}" class="nav-link {{ request()->is('dashboard/cameras/create') ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Qo'shish</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('cameras.index') }}" class="nav-link {{ ( request()->is('dashboard/cameras') || request()->is('dashboard/cameras/filter') ) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ko'rish</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
