<!DOCTYPE html>
<html lang="en">

<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Online kuzatuv</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/user/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/user/css/style.css') }}">
    <link href="https://vjs.zencdn.net/7.2.3/video-js.css"/>
</head>

<body>
<header id="home" class="section">
    <div class="header_main">
        <!-- header inner -->
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                        <div class="full">
                            <div class="center-desk">
                                <div class="logo"><a href="#home"><img src="{{ asset('web/user/images/logo.png') }}" style="max-width: 100%;"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                        <div class="menu-area">
                            <div class="limit-box">
                                <nav class="main-menu">
                                    <ul class="menu-area-main">
                                        <li><a href="/">Bosh sahifa</a></li>

                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header inner -->
        <section>
            <div class="bannen_inner">
                <div class="container">
                    <div class="row marginii">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="taital_main">

                            </div>
                            <h1 class="web_text"><strong>Onlayn kuzatuv</strong></h1>
                            <p class="donec_text">Alohida iqtidor talab etiladigan bakalavriat ta’lim yo‘nalishlariga kirish
                                uchun kasbiy (ijodiy) imtihon
                                jarayonlari</p>
                            <a class="get_bg" href="http://camera.buxdu.uz" role="button">Batafsil</a>

                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="img-box">
                                <figure><img src="{{ asset('web/user/images/woofer.png') }}" alt="img" style="max-width: 100%;"></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>
<!-- banner end -->
<!-- choose start -->
<div id="about" class="choose_section">
    <div class="container">
        <div class="col-sm-12">
            <h1 class="choose_text"><span class="color">{{ $cameras[0]->bolim_name }}</span></h1>
        </div>
    </div>
</div>
<div class="choose_section_2">

    <div class="container">
        <div class="row">
            @foreach($cameras as $key => $camera)
                <div class="col-md-4 col-sm-12 mb-4">
                    <div class="card powers">
                        @if($camera->sub_bolim_name != '0' )
                            <h2 class="totaly_text">{{ $camera->sub_bolim_name }}</h2>
                        @endif
                        <div class="ifr">
                            <video class="camera" style="width: 100%;" id="videoPlayer_{{$key}}" autoplay controls muted playsinline></video>
                            <canvas id="canvas" class="d-none"></canvas>
                        </div>
                        <h2 class="totaly_text">{{ $camera->name }}</h2>
                    </div>
                </div>
                <input type="hidden" value="{{ $camera->uuid }}" id="uuid_{{$key}}">
                <input type="hidden" value="0" id="channel_{{$key}}">
            @endforeach
        </div>
        <div class="d-flex align-items-center justify-content-center" style="margin-top: 20px;">
            {{ $cameras->links() }}
        </div>

    </div>
</div>
<br>

<div class="copyright_main">
    <div class="container">
        <p class="copy_text">© 2022 <a href="https://buxdu.uz">Raqamli ta'lim texnologiyalar markazi.</a></p>
    </div>

</div>



<!-- contact end -->
<!-- Javascript files-->
<script src="{{ asset('web/user/js/jquery.min.js') }}"></script>
<script src="{{ asset('web/user/js/popper.min.js') }}"></script>
<script src="{{ asset('web/user/js/bootstrap.bundle.min.js') }}"></script>


<script src="{{ asset('web/user/js/jquery-3.0.0.min.js') }}"></script>
<script src="{{ asset('web/user/js/plugin.js') }}"></script>
<!-- sidebar -->
<script src="{{ asset('web/user/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('web/user/js/custom.js') }}"></script>
<!-- javascript -->

<script src="{{ asset('web/user/hlsjs/hls.min.js') }}"></script>
<script>

    $(document).ready(()=>{
        startPlay();
    });
    function startPlay() {
        for (let k = 0; k < document.querySelectorAll('.camera').length; k++) {
            let uuid=$('#uuid_'+k).val();
            let channel=$('#channel_'+k).val();
            let url ='http://{{ $api_url }}:{{ $api_port }}/stream/' + uuid + '/channel/'+channel+'/hls/live/index.m3u8';
            if ($("#videoPlayer_"+k)[0].canPlayType('application/vnd.apple.mpegurl')) {
                $("#videoPlayer_"+k)[0].src = url;
                $("#videoPlayer_"+k)[0].load();
            } else if (Hls.isSupported()) {
                let hls=new Hls({manifestLoadingTimeOut:60000});
                hls.loadSource(url);
                hls.attachMedia($("#videoPlayer_"+k)[0]);
                hls.on('error', function(e) {
                    console.log(e);
                })
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Your browser don`t support hls '
                });
            }

        }
        let canplaythroughTime=null;//time when  buffer have enaugh to play

        $("#videoPlayer_"+k)[0].addEventListener('progress', () => {
            if (typeof document.hidden !== "undefined" && document.hidden && canplaythroughTime!=null) {
                //no sound, browser paused video without sound in background
                $("#videoPlayer_"+k)[0].currentTime = $("#videoPlayer_"+k)[0].buffered.end(($("#videoPlayer_"+k)[0].buffered.length - 1)) - canplaythroughTime;
            }
        });

        $("#videoPlayer_"+k)[0].addEventListener('canplaythrough', () => {
            if(canplaythroughTime==null){
                canplaythroughTime=$("#videoPlayer_"+k)[0].buffered.end(($("#videoPlayer_"+k)[0].buffered.length - 1));
            }
        });

        $("#videoPlayer_"+k)[0].addEventListener('loadeddata', () => {
            $("#videoPlayer_"+k)[0].play();
            makePic();
        });

        $("#videoPlayer_"+k)[0].onerror = function() {
            console.log("Error " + $("#videoPlayer_"+k)[0].error.code + "; details: " + $("#videoPlayer_"+k)[0].error.message);
        }
    }
</script>
</body>

</html>
