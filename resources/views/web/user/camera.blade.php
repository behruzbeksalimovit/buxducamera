
{{--@extends('web.layout.user')--}}
{{--@section('content')--}}
{{--    <div class="row pt-2">--}}
{{--        @foreach($cameras as $camera)--}}
{{--            <div class="col-md-3">--}}
{{--                <div class="card">--}}
{{--                    <div class="card-header">--}}
{{--                        <h3>--}}
{{--                            <a href="">{{ $camera->name }}</a>--}}
{{--                        </h3>--}}
{{--                    </div>--}}
{{--                    <!-- /.card-header -->--}}
{{--                    <div class="card-body p-2">--}}

{{--                        <video id="hls-example" class="video-js vjs-default-skin" width="445" height="300" controls autoplay>--}}
{{--                            <source  type="application/x-mpegURL" src="http://{{ $api_url }}:{{ $api_port }}/stream/{{ $camera->uuid }}/channel/0/hls/live/index.m3u8">--}}
{{--                        </video>--}}
{{--                    </div>--}}
{{--                    <!-- /.card-body -->--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        @endforeach--}}
{{--        <div>--}}
{{--            {{ $cameras->links() }}--}}
{{--        </div>--}}
{{--    </div>--}}
{{--@endsection--}}
{{--@push('script')--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>--}}
{{--    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>--}}
{{--    <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>--}}

{{--    <script>--}}
{{--        let camera = document.querySelectorAll('#hls-example');--}}
{{--        camera.forEach(camera=>{--}}
{{--            var player = videojs(camera);--}}
{{--            player.play()--}}
{{--        });--}}
{{--    </script>--}}
{{--@endpush--}}








<!DOCTYPE html>
<html lang="en">
<head>
    <!-- basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- mobile metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- site metas -->
    <title>Online kuzatuv</title>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/user/css/bootstrap.min.css') }}">
    <!-- style css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('web/user/css/style.css') }}">
    <!-- Responsive-->
    <link rel="stylesheet" href="{{ asset('web/user/css/responsive.css') }}">
    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="{{ asset('web/user/css/jquery.mCustomScrollbar.min.css') }}">
    <!-- Tweaks for older IEs-->



    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-1X41XQGJZ5"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-1X41XQGJZ5');
    </script>


</head>
<body>
<header id="home" class="section">
    <div class="header_main">
        <!-- header inner -->
        <div class="header">
            <div class="container">
                <div class="row">
                    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col logo_section">
                        <div class="full">
                            <div class="center-desk">
                                <div class="logo"><a href="http://buxdu.uz"><img src="{{ asset('web/user/images/logo.png') }}" style="max-width: 100%;"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-9 col-sm-9">
                        <div class="menu-area">
                            <div class="limit-box">
                                <nav class="main-menu">
                                    <ul class="menu-area-main">
                                        <li><a href="#home">Bosh sahifa</a></li>
                                        <li><a href="#about">Dasturlar</a></li>
                                        <li><a href="#service">Kameralar</a></li>
                                        <li><a href="{{ route('user.loginPage') }}">Kirish</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end header inner -->
        <section >
            <div class="bannen_inner">
                <div class="container">
                    <div class="row marginii">
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="taital_main">

                            </div>
                            <h1 class="web_text"><strong>Onlayn kuzatuv</strong></h1>
                            <p class="donec_text">Vatel (2022-2023) kirish imtihoni.</p>
                            <a class="get_bg" href="#service" role="button">Batafsil</a>

                        </div>
                        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                            <div class="img-box">
                                <figure><img src="{{ asset('web/user/images/woofer.png') }}" alt="img" style="max-width: 100%;"></figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</header>
<!-- banner end -->
<!-- choose start -->
<div id="about" class="choose_section">
    <div class="container">
        <div class="col-sm-12">

            <h1 class="choose_text">Statistika</h1>

        </div>
    </div>
</div>
<div class="choose_section_2">
    <div class="container">
        <div class="mb-5">
            <div class="row" style="margin-bottom: 60px;">
                <div class="col-sm-4">
                    <div class="power">

                        <div class="icon"><a href="https://buxdu.uz/media/qabul/2022/kvota/dasturlar/Axborot_xizmati_va_jamoatchilik_bilan_aloqalar.pdf"><img src="{{ asset('web/user/images/Group 9390.png') }}"></a></div>
                        <h2 class="totaly_text">Bo'limlar</h2>
                        <h1 class="making">{{ $section_count }}</h1>
                    </div>
{{--                    <div class="btn_main">--}}
{{--                        <button type="button" class="read_bt"><a href="https://buxdu.uz/media/qabul/2022/kvota/dasturlar/Axborot_xizmati_va_jamoatchilik_bilan_aloqalar.pdf">Batafsil</a></button>--}}
{{--                    </div>--}}
                </div>
                <div class="col-sm-4">
                    <div class="power">
                        <div class="icon"><a href="https://buxdu.uz/media/qabul/2022/kvota/dasturlar/bosma_OAV.pdf"><img src="{{ asset('web/user/images/Group 9390.png') }}"></a></div>
                        <h2 class="totaly_text">Ichki bo'limlar</h2>
                        <h1 class="making">{{ $sub_section }}</h1>

                    </div>
{{--                    <div class="btn_main">--}}
{{--                        <button type="button" class="read_bt"><a href="https://buxdu.uz/media/qabul/2022/kvota/dasturlar/bosma_OAV.pdf">Batafsil</a></button>--}}
{{--                    </div>--}}
                </div>
                <div class="col-sm-4">
                    <div class="power">
                        <div class="icon"><a href="https://buxdu.uz/105-qabul-2022/4679/4679-sport-faoliyati/"><img src="{{ asset('web/user/images/Group 9389.png') }}"></a></div>
                        <h2 class="totaly_text">Kameralar</h2>
                        <h1 class="making">{{ $camera_count }}</h1>
                    </div>
{{--                    <div class="btn_main">--}}
{{--                        <button type="button" role="button"  class="read_bt"><a href="https://buxdu.uz/105-qabul-2022/4679/4679-sport-faoliyati/">Batafsil</a></button>--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</div>

<div id="service" class="choose_section">
    <div class="container">
        <div class="col-sm-12">
            <h1 class="choose_text">Kuzatuv<span class="color"> kameralar</span></h1>

        </div>
    </div>
</div>
<div class="choose_section_2" style="margin-bottom: 134px;">
    <div class="container">
        <div class="row">
            @foreach($cameras as $camera)
                <div class="col-sm-4">
                    <div class="about_inner">
                        <p class="totaly_text"> </p>
                        <div class="icon"><a href="{{ route('user.cameraSub', $camera->sub_id) }}"><img src="{{ asset('web/user/images/icon-7.png') }}"></a></div>
                        <h2 class="totaly_text">{{ $camera->bolim_name }}</h2>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="d-flex align-items-center justify-content-center" style="margin-top: 20px;">
            {{ $cameras->links() }}
        </div>
    </div>
</div>
<div class="contact_section_3">
    <div class="container">
        <div class="contact_taital" style="margin-top: 0;">
            <div class="row web">
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="map_main">
                        <img src="{{ asset('web/user/images/map-icon.png') }}">
                        <span class="londan_text">Buxoro sh. M.Iqbol 11</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="map_main">
                        <img src="{{ asset('web/user/images/phone-icon.png') }}">
                        <span class="londan_text">+998(65)221-29-14</span>
                    </div>
                </div>
                <div class="col-sm-6 col-md-6 col-lg-4">
                    <div class="map_main">
                        <img src="{{ asset('web/user/images/email-icon.png') }}">
                        <span class="londan_text">buxdu_rektor@buxdu.uz</span>
                    </div>
                </div>
            </div>
        </div>
{{--        <div class="contact_product">--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-6 col-md-6 col-lg-2">--}}

{{--                </div>--}}
{{--                <div class="col-sm-6 col-md-6 col-lg-4">--}}
{{--                    <h1 class="useful_text">Menyu</h1>--}}
{{--                    <div class="menu">--}}
{{--                        <ul>--}}
{{--                            <li><a href="#home"><img src="{{ asset('web/user/images/bulit-icon.png') }}" style="padding-right: 10px;">Bosh sahifa</a></li>--}}
{{--                            <li><a href="#about"><img src="{{ asset('web/user/images/bulit-icon.png') }}" style="padding-right: 10px;">Dasturlar</a></li>--}}
{{--                            <li><a href="#service"><img src="{{ asset('web/user/images/bulit-icon.png') }}" style="padding-right: 10px;">Kameralar</a></li>--}}
{{--                            <li><a href="#contact"><img src="{{ asset('web/user/images/bulit-icon.png') }}" style="padding-right: 10px;">Qayta aloqa</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-12 col-md-12 col-lg-6">--}}
{{--                    <h1 class="useful_text">Havolalar</h1>--}}
{{--                    <div class="menu multi_column_menu">--}}
{{--                        <ul>--}}
{{--                            <li class="footer_menu"><a href="http://buxdu.uz"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Rasmiy veb-sayt</a></li>--}}
{{--                            <li class="footer_menu"><a href="http://hemis.buxdu.uz"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Hemis tizimi</a></li>--}}
{{--                            <li class="footer_menu"><a href="http://elib.buxdu.uz"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Kutubxona</a></li>--}}
{{--                            <li class="footer_menu"><a href="http://uniwork.buxdu.uz"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Uniwork tizimi</a></li>--}}
{{--                            <li class="footer_menu"><a href="http://moodle.buxdu.uz"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Masofaviy o`qitish tizimi</a></li>--}}
{{--                            <li class="footer_menu"><a href="https://interactive.buxdu.uz/"><img src="{{ asset('web/user/images/bulit-icon.png') }}" class="footer_menu">Interaktiv xizmatlar</a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                    <div class="input-group mb-3 margin-top-30">--}}
{{--                        <input type="text" class="form-control" placeholder="Enter you email">--}}
{{--                        <div class="input-group-append">--}}
{{--                            <button class="subsrcibe_bt" type="Subscribe"><a href="#">SUBSCRIBE</a></button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="icon_main">--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-12">--}}
{{--                    <div class="menu_text">--}}
{{--                        <ul>--}}
{{--                            <li><a href="#"><img src="{{ asset('web/user/images/fb-icon.png') }}"></a></li>--}}
{{--                            <li><a href="#"><img src="{{ asset('web/user/images/twitter-icon.png') }}"></a></li>--}}
{{--                            <li><a href="#"><img src="{{ asset('web/user/images/in-icon.png') }}"></a></li>--}}
{{--                            <li><a href="#"><img src="{{ asset('web/user/images/google-icon.png') }}"></a></li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
</div>
<div class="copyright_main">
    <div class="container">
        <p class="copy_text">© 2022 <a href="https://buxdu.uz">Raqamli ta'lim texnologiyalar markazi.</a></p>
{{--        <center><a href="https://livetrafficfeed.com/hit-counter" data-root="0" data-unique="0" data-style="14" data-min="3" data-start="1" id="LTF_hitcounter">Free Hit Counter</a><script type="text/javascript" src="//cdn.livetrafficfeed.com/static/hitcounterjs/live.js"></script></center><noscript><a href="https://livetrafficfeed.com/hit-counter">Free Hit Counter</a></noscript>--}}
    </div>
</div>


<!-- contact end -->
<!-- Javascript files-->
<script src="{{ asset('web/user/js/jquery.min.js') }}"></script>
<script src="{{ asset('web/user/js/popper.min.js') }}"></script>
<script src="{{ asset('web/user/js/bootstrap.bundle.min.js') }}"></script>


<script src="{{ asset('web/user/js/jquery-3.0.0.min.js') }}"></script>
<script src="{{ asset('web/user/js/plugin.js') }}"></script>
<!-- sidebar -->
<script src="{{ asset('web/user/js/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script src="{{ asset('web/user/js/custom.js') }}"></script>

</body>
</html>





