@extends('web.layout.master')
@section('content')
    <div class="row pt-4">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">Kamera ko'rish</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" action="{{ route('camera.rektor') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">

                        <div class="form-group">
                            <label for="parent">Bo'lim</label>
                            <select name="sub_id" id="parent" class="form-control section select2bs4" style="width: 100%; height: 100%">
                                <option hidden value="0">Bo'limni tanlang!</option>
                                @foreach($sub_nulls as $section)
                                    <option data-bolim-rahbar-id="{{ $section->rahbar_id }}" value="{{ $section->id }}">{{ $section->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group sec">
                            <label for="section_id">Sub bo'limlar</label>
                            <select name="section_id" id="section_id" class="form-control section_sub">
                                <option hidden selected value="0">Sub bo'limni tanlang!</option>
                            </select>
                        </div>

{{--                        <div class="form-group rahbar">--}}
{{--                            <label for="rahbar_id">Rahbarlar</label>--}}
{{--                            <select name="rahbar_id" id="rahbar_id" class="form-control rahbar_id">--}}
{{--                                <option hidden selected value="0">Rahbarni tanlang!</option>--}}
{{--                            </select>--}}
{{--                        </div>--}}

                    </div>
                    <!-- /.card-body -->
                    <div id="add_input">
                        <input type="text" name="rahbarlar[]" id="rahbarlar" class="d-none" value="2555">
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">Ko'rish</button>
                    </div>
                </form>
            </div>
            <!-- /.card -->
        </div>
    </div>
@endsection
@push('script')
    <script>
        $(document).ready(function(){
            var rahbar_bolim_id = '';
            var rahbar_sub_id = '';
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $(".section").change(function (){
                $(".bolim").remove();
                $('.section_sub').html('');
                let sub_id = $(this).val();
                let options = $('.section option');
                for(let i=0; i<options.length; i++)
                {
                    if(sub_id === options[i].getAttribute('value')){
                        rahbar_bolim_id = options[i].getAttribute('data-bolim-rahbar-id');
                    }
                }
                $.ajax({
                    url: '{{ route('ajax.getSubIdRektor') }}',
                    method: 'POST',
                    data: {
                        sub_id: sub_id,
                    },
                    success: function (response) {
                        var formoption = "";
                        $.each(response.departments, function(index, department) {
                            formoption += "<option data-sub-rahbar-id='"+department.rahbar_id+"' data-section='"+ department.sub_id+"' value='"+department.id+"'>"+ (department.name) +"</option>";
                        });
                        $("#add_input").append(`
                            <input name="rahbarlar[]" class="d-none bolim" value="${rahbar_bolim_id}"/>
                        `);

                        $(".section_sub").append(formoption);
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
            });

            {{--$(".section_sub").change(function (){--}}
            {{--    $(".sub__bolim").remove();--}}
            {{--    $('.rahbar_id').html('');--}}
            {{--    let id = $(this).val();--}}
            {{--    let sub_id = $(this).attr('data-section');--}}
            {{--    let options = $('.section_sub option');--}}
            {{--    for(let i=0; i<options.length; i++)--}}
            {{--    {--}}
            {{--        if(id === options[i].getAttribute('value')){--}}
            {{--            rahbar_sub_id = options[i].getAttribute('data-sub-rahbar-id');--}}
            {{--        }--}}
            {{--    }--}}
            {{--    $.ajax({--}}
            {{--        url: '{{ route('ajax.getRahbarId') }}',--}}
            {{--        method: 'POST',--}}
            {{--        data: {--}}
            {{--            id: id,--}}
            {{--            sub_id: sub_id,--}}
            {{--        },--}}
            {{--        success: function (response) {--}}
            {{--            var formoption = "";--}}
            {{--            $.each(response.departments, function(index, department) {--}}
            {{--                formoption += "<option data-section-rahbar='"+ department.sub_id+"' value='"+department.rahbar_id+"'>"+ department.rahbar +"</option>";--}}
            {{--            });--}}
            {{--            $("#add_input").append(`--}}
            {{--                <input name="rahbarlar[]" class="d-none sub__bolim" value="${rahbar_sub_id}"/>--}}
            {{--            `);--}}
            {{--            $(".rahbar_id").append(formoption);--}}
            {{--        },--}}

            {{--        // error: function (e) {--}}
            {{--        //     console.log(e);--}}
            {{--        // }--}}
            {{--    });--}}
            {{--});--}}

            $('select[name="sub_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='section_id']").val("");
                $(".sec").removeClass('d-none');

                $('select[name="section_id"] option').each(function() {
                    if ($(this).attr('data-section') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

            $('select[name="section_id"]').change(function() {
                var parent = $(this).val();

                $("select[name='rahbar_id']").val("");
                $(".rahbar").removeClass('d-none');

                $('select[name="rahbar_id"] option').each(function() {
                    if ($(this).attr('data-section-rahbar') == parent) {
                        $(this).show();
                    } else {
                        $(this).hide();
                    }

                });
            });

        });
    </script>
@endpush


