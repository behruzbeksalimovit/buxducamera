@extends('web.layout.master')
@section('content')

    <div class="row pt-2">
        @foreach($cameras as $camera)
            <div class="col-md-3">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            <a>{{ $camera->name }}</a>
                        </h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-2">
                        <video id="hls-example" class="video-js vjs-default-skin" width="380" height="300" controls autoplay>
                            <source  type="application/x-mpegURL" src="http://{{ $api_url }}:{{ $api_port }}/stream/{{ $camera->uuid }}/channel/0/hls/live/index.m3u8">
                        </video>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        @endforeach
        <div class="d-flex align-items-center justify-content-center" style="margin-top: 20px;">
            {{ $cameras->links() }}
        </div>
    </div>
@endsection
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
    {{--<script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>--}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
    <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
    <script>
        let camera = document.querySelectorAll('#hls-example');
        camera.forEach(camera=>{
            var player = videojs(camera);
            player.play()
        });
    </script>
@endpush

