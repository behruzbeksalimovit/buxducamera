<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('rector') }}" class="brand-link">
        <img src="{{ asset('dashboard/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">BuxDU Camera</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

        @if(\Illuminate\Support\Facades\Session::get('id') == env('REKTOR_ID'))
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item {{ (request()->is('rector/cameras/cameras')) ? 'menu-is-opening menu-open' : '' }}">
                    <a href="#" class="nav-link {{ (request()->is('rector/cameras')) ? 'active' : '' }}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>
                            Bo'limlar
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview {{ ( request()->is('rector/rektor') || request()->is('rector/rektor/section')) ? 'd-block' : '' }}">
                        <li class="nav-item">
                            <a href="{{ route('camera.rektorSection') }}" class="nav-link {{ ( request()->is('rector/rektor') || request()->is('rector/rektor/section')) ? 'active' : '' }}">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Ko'rish</p>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
            @endif
    </div>
    <!-- /.sidebar -->
</aside>
